# O programu

Program slouží k výpočtu determinantu matice. Implementace je založená na
Gaussově eliminančí metodě (LU dekompozice). Aplikace na vstupu dostane čtvercovou
matici a na výstupu vypíše hodnotu jejího determinantu.

Velikost vstupní matice n×n je zadána v argumentu programu. Dále uživatel volí počet matic a způsob výpočtu - paralelní zpracování za pomocí vláken či nikoliv.

## Výpočet
Výpočet determinantu je rozdělen do dvou částí.

1. LU dekompozice prostřednictvím Doolittlova algoritmu
    - tento algoritmus přijímá na vstupu matici, kterou podle diagonály rozdělí na horní (upper)/dolní(lower) trojúhelníkový tvar. V případě LT obsahuje matice nad jednotkovou diagonálou nulové hodnoty, v případě UT obsahuje nulové prvky pod diagonálou.
    - [Imgur](https://i.imgur.com/6PP0aMt.png)
2. Výpočet determinantu z dekomponované matice
    - determinant matice je následně možné získat ze získané diagonály UT.

## Implementace
V programu je definován datový typ `Matrix`, který uchovává 2D pole vektorů s daty. Dále každá matice v sobě obsahuje hodnotu determinantu a počet řádků (resp. sloupců).

Dále obsahuje pomocnou třídu `Helper`, ve které jsou naimplementovány metody pro generování vektorů čísel typu `double` a zápis do souboru.

Počítaná matice je zapisována do souboru `input.txt`.

## Manipulace s programem
Program přijímá 3 argumenty:
1. Velikost matice v rozsahu <1, 1000>
2. Typ výpočtu: (0) - sekvenční, (1) - paralelní
3. Počet matic v rozsahu <1, 1000>

## Měření

Měření proběhlo vůči kódu v commitu `2346f0`, na notebooku MacBook Pro 2017
s procesorem 2,3 GHz Intel Core i5-7360U, v programu Xcode 10.1.

Výstup z měření

|  N  | n×n | ST[s] | MT[s] |
|-----|-----|-------|-------|
|   1 |  1  | 0,000 | 0,000 |
|   1 |1000 | 4,416 | 4,733 |
|  10 |1000 |46,628 |20,730 |
| 100 |  2  | 0,002 | 0,005 |
| 100 |  10 | 0,009 | 0,007 |
| 100 | 100 | 0,394 | 0,210 |
| 100 | 250 | 6,888 | 2,661 |
|1000 |   1 | 0,026 | 0,138 |
|1000 |  10 | 0,093 | 0,107 |
|1000 | 100 | 7,547 | 1,984 |
