//
//  Helpers.hpp
//  TermProject
//
//  Created by Kateřina Dlouhá on 27/01/2019.
//  Copyright © 2019 Kateřina Dlouhá. All rights reserved.
//

#ifndef Helpers_hpp
#define Helpers_hpp

#include <stdio.h>
#include <iomanip>
#include <vector>

namespace pjc {
    class Helpers {
        public:
        double getRandomDouble(int min, int max);
        std::vector<std::vector<double>> getRandomVector(std::size_t size, bool isNull);
        void insertMatrixValuesIntoFile(std::vector<std::vector<double>>& values);
    };
}

#endif /* Helpers_hpp */
