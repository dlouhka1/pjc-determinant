//
//  Determinant.cpp
//  TermProject
//
//  Created by Kateřina Dlouhá on 27/01/2019.
//  Copyright © 2019 Kateřina Dlouhá. All rights reserved.
//

#include <stdlib.h>
#include <iomanip>
#include <iostream>
#include <cmath>
#include <vector>
#include <exception>
#include <thread>

#include "Matrix.hpp"
#include "Helpers.hpp"

namespace pjc {
    Helpers helper;
    
    Matrix::Matrix(std::vector<std::vector<double>> data, std::size_t numer_of_rows) : data(data), det(0), numberOfRows(numer_of_rows) {}
    
    std::size_t Matrix::getNumberOfRows() {
        return this->numberOfRows;
    }
    
    // Inspired by https://en.wikipedia.org/wiki/LU_decomposition#Computing_the_determinant
    // https://en.wikipedia.org/wiki/LU_decomposition#Doolittle_algorithm
    Matrix Matrix::luDecomposition() {
        Matrix upperMatrix(helper.getRandomVector(this->getNumberOfRows(), true), this->numberOfRows);
        Matrix lowerMatrix(helper.getRandomVector(this->getNumberOfRows(), true), this->numberOfRows);
        
        for (int i = 0; i < this->getNumberOfRows(); i++) {

            for (int k = i; k < this->getNumberOfRows(); k++) {
                
                long double sum = 0;
                for (int j = 0; j < i; j++)
                    sum += (lowerMatrix.data[i][j] * upperMatrix.data[j][k]);
                
                upperMatrix.data[i][k] = this->data[i][k] - sum;
            }
            
            for (int k = i; k < this->getNumberOfRows(); k++) {
                if (i == k)
                    lowerMatrix.data[i][i] = 1; // Diagonal as 1
                else {
                    
                    long double sum = 0;
                    for (int j = 0; j < i; j++)
                        sum += (lowerMatrix.data[k][j] * upperMatrix.data[j][i]);
                    
                    lowerMatrix.data[k][i] = (this->data[k][i] - sum) / upperMatrix.data[i][i];
                }
            }
        }
        
        return upperMatrix;

    }

    long double Matrix::countDeterminant() {
        this->det = 1; // in case we have size of matrix 0
        
        Matrix upperMatrix = this->luDecomposition();

        for (int i = 0; i < upperMatrix.getNumberOfRows(); ++i) {
            if (upperMatrix.numberOfRows > i) {
                this->det *= upperMatrix.data[i][i];
            }
        }
        
        return this->det;
    }

    void Matrix::print() {
        for (std::size_t i = 0; i < data.size(); ++i) {
            for (std::size_t j = 0; j < data.size(); ++j) {
                std::cout << std::setprecision(6);
                std::cout << std::setw(12) << data[i][j] << std::setw(12);
            }
            std::cout << std::endl << std::endl;
        }
        std::cout << std::endl;
    }
}
