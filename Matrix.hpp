//
//  Determinant.hpp
//  TermProject
//
//  Created by Kateřina Dlouhá on 27/01/2019.
//  Copyright © 2019 Kateřina Dlouhá. All rights reserved.
//

#ifndef Determinant_hpp
#define Determinant_hpp

#include <stdio.h>
#include <iomanip>
#include <vector>

namespace pjc {
    class Matrix {
    private:
        std::vector<std::vector<double>> data;
        std::size_t numberOfRows;
        long double det;
    public:
        Matrix();
        Matrix(std::vector<std::vector<double>> data, std::size_t numer_of_rows);

        std::vector<std::vector<double>> getMatrix() const { return this->data; };

        void print();
        Matrix luDecomposition();
        long double countDeterminant();
        long double getDeterminant();
        std::size_t getNumberOfRows();
    };

}

#endif /* Determinant_hpp */
