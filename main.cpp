//
//  main.cpp
//  TermProject
//
//  Created by Kateřina Dlouhá on 27/01/2019.
//  Copyright © 2019 Kateřina Dlouhá. All rights reserved.
//

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <iomanip>
#include <thread>
#include <atomic>
#include <vector>
#include <random>
#include <chrono>
#include <algorithm>
#include <string>
#include <cstring>
#include <utility>
#include <future>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "Helpers.hpp"
#include "Matrix.hpp"

pjc::Helpers helper;

namespace timer {
    auto now() {
        return std::chrono::high_resolution_clock::now();
    }
    
    template <typename TimePoint>
    long long to_ms(TimePoint tp) {
        return std::chrono::duration_cast<std::chrono::milliseconds>(tp).count();
    }
    
}

void print_usage(std::string const& exe_name) {
    std::clog << "Program accept 3 arguments:\n (1) Size of matrix <1, 1000>.\n (2) Threading: 0 for single thread, 2 for multi thread.\n (3) Number of matrixes from <1, 1000>.\n";
}

bool is_help(std::string const& argument) {
    return argument == "--help" || argument == "-h";
}

void multipleThreadCount(std::vector<pjc::Matrix>& matrices) {
    std::cout << "-- Multi thread --" << std::endl;
    
    std::vector<std::future<long double>> futures;
    
    auto start = timer::now();
    
    for (pjc::Matrix& matrix : matrices) {
        futures.push_back(std::async(std::launch::async, [&matrix]() {
            return matrix.countDeterminant();
        }));
    }
    
    for (auto &future : futures) {
        future.wait();
        std::cout << std::fixed;
        std::cout << std::setprecision(3);
        std::cout << std::endl << "Determinant is " << future.get() << std::endl;
    }
    
    auto end = timer::now();
    
    std::cout << "Time: " << timer::to_ms(end-start) << " ms" << std::endl;
}

void singleThread(std::vector<pjc::Matrix>& matrices) {
    std::cout << "-- Single thread --" << std::endl;
    
    auto startST = timer::now();
    
    std::cout << "Counting determinant... " << std::endl;
    
    for (pjc::Matrix& matrix : matrices) {
        long double determinantNumber = matrix.countDeterminant();
        std::cout << std::fixed;
        std::cout << std::setprecision(3);
        std::cout << std::endl << "Determinant is " << determinantNumber << std::endl;
    }
    
    auto endST = timer::now();
    
    std::cout << "Time: " << timer::to_ms(endST-startST) << " ms\n\n";
}

int main(int argc, char *argv[]) {
    std::size_t numberOfMatrices = 100;
    std::size_t matrixSize = 10;
    int isMultipleThread = -1;
    
    std::cout << "Program for counting determinant\n" ;
    
    if (std::any_of(argv, argv+argc, is_help)) {
        print_usage(argv[0]);
        return 0;
    }

    if (argc == 1) {
        std::clog << "No arguments given, using default values.\n";

    } else if (argc > 4) {
        std::clog << "Too many arguments given.\n";
        print_usage(argv[0]);
        return 1;
    } else {
        for (int i = 1; i < argc; i++) {
            if (!isdigit(*argv[i])) {
                std::clog << "Invalid arguments. Run program with --help or -h for more information.\n";
                return 2;
            }
        }
        
        if (argc > 1 and (std::stoi(argv[1]) < 1 or std::stoi(argv[1]) > 1000)) {
            std::clog << "The first argument for matrices size, '" << argv[1] <<  "', is not in range <1, 1000>.\n";
            return 3;
        }
        
        if (argc > 2 and (std::stoi(argv[2]) != 0 and std::stoi(argv[2]) != 1)) {
            std::clog << "The second argument for threading is not 0 (single thread) or 1 (multi thread).\n";
            return 4;
        }
            
        if (argc > 3 and (std::stoi(argv[3]) < 1 or std::stoi(argv[3]) > 1000)) {
            std::clog << "The third argument for number of matrices, '" << argv[3] <<  "', is not in range <1, 1000>.\n";
            return 4;
        }
    }

    if (argc > 1) {
        matrixSize = std::stoi(argv[1]);
        
        if (argc > 2) {
            isMultipleThread = std::stoi(argv[2]);
        }
        
        if (argc > 3) {
            numberOfMatrices = std::stoi(argv[3]);
        }
    }
    
    std::cout << "Size of matrixes: " << matrixSize << std::endl;
    std::cout << "Multithreading: " << isMultipleThread << std::endl;
    std::cout << "Number of matrixes: " << numberOfMatrices<< std::endl << std::endl;
    
    std::vector<pjc::Matrix> matrices;
    
    for (int i = 0; i < numberOfMatrices; i++) {
        matrices.push_back(pjc::Matrix(helper.getRandomVector(matrixSize, false), matrixSize));
    }
    
    if (isMultipleThread == 0) {
        singleThread(matrices);
    }
    
    else if (isMultipleThread == 1) {
        multipleThreadCount(matrices);
    }
    
    else {
        singleThread(matrices);
        multipleThreadCount(matrices);
    }
}
