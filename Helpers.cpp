//
//  Helpers.cpp
//  TermProject
//
//  Created by Kateřina Dlouhá on 27/01/2019.
//  Copyright © 2019 Kateřina Dlouhá. All rights reserved.
//

#include <random>
#include <fstream>
#include <iterator>
#include <random>
#include <vector>

#include "Helpers.hpp"

namespace pjc {

    double Helpers::getRandomDouble(int min, int max) {
        static std::minstd_rand mt{ std::random_device{}() };
        static std::uniform_real_distribution<> dist(-min, max);
        return dist(mt);
    }

    std::vector<std::vector<double>> Helpers::getRandomVector(std::size_t size, bool isNull) {
        std::vector<std::vector<double>> data;
        
        for (std::size_t i = 0; i < size; i++) {
            std::vector<double> row;

            for (std::size_t l = 0; l < size; l++) {
                if (isNull) {
                    row.push_back(0);
                }
                else {
                    double value = getRandomDouble(10, 10);
                    row.push_back(value);
                }
            }

            data.push_back(row);
        }
        
        if (!isNull) {
            insertMatrixValuesIntoFile(data);
        }
        
        return data;
    }

    void Helpers::insertMatrixValuesIntoFile(std::vector<std::vector<double>>& values) {
        std::ofstream file("input.txt", std::ofstream::app);
        
        for (size_t i = 0; i < values.size(); i++) {
            for (size_t l = 0; l < values[i].size(); l++) {
                if (l + 1 < values[i].size()) {
                    file << std::setw(8) << values[i][l] << " | " << std::setw(8);
                }
                else file << values[i][l];
            }
            file << std::endl;
            for (int i = 0; i < (values.size() * 11); ++i) {
                file << "-";
            }
            file << std::endl;
        }
        
        file << std::endl << std::endl;

        file.close();
    }
}
